# README #

Supporting code for

Peter E. van Keken, Ikuko Wada, Nathan Sime and Geoffrey A. Abers,
*Thermal Structure of the Forearc in Subduction Zones: A Comparison of Methodologies*,
[Geochemistry, Geophysics, Geosystems, 2019, 20(7), 3268–3288](https://doi.org/10.1029/2019GC008334).


# Installation #

## Docker container ##

[![Docker Repository on Quay](https://quay.io/repository/natesime/subduction-zone-forearc-thermal-structure/status "Docker Repository on Quay")](https://quay.io/repository/natesime/subduction-zone-forearc-thermal-structure)

Install docker and get the latest image with

```bash
docker run -ti quay.io/natesime/subduction-zone-forearc-thermal-structure
```

See the FEniCS in Docker [documentation](https://fenics.readthedocs.io/projects/containers/en/latest/) for troubleshooting.

## Custom installation ##

Clone the repository and run as normal on your local system or in your own container, e.g:

```bash
git clone https://bitbucket.org/nate-sime/subduction-zone-forearc-thermal-structure.git
cd subduction-zone-forearc-thermal-structure/src
python3 subduction_forearc.py
```

Dependencies:

1. python3.6
2. [FEniCS project 2018.1](https://fenicsproject.org/)
3. [PETSc 3.9.1](https://www.mcs.anl.gov/petsc/)
4. [gmsh 4.0](http://gmsh.info/)
5. [pygmsh](https://pypi.org/project/pygmsh/)
6. [meshio](https://pypi.org/project/meshio/)
7. [h5py](https://www.h5py.org/)

# Running #

## Workflow ##

1. Navigate to `subduction-zone-forearc-thermal-structure/src`
2. `python3 subduction_mesh_generator.py` to generate the subduction mesh.
3. `python3 subduction_forearc.py` to run the simulation.
5. `python3 post_processing.py` to generate example postprocessing of the data in `src/results`.

## Parameters ##

The subduction zone mesh generator parameters in `subduction_mesh_generator.py` are
tabulated as follows:

* `delta` Dip angle (rad)
* `slab_depth` Slab depth (km)
* `z_tau` Shear heating peak temperature depth (km)
* `z0` Shear heating depth (km)
* `lcar` Characteristic length of mesh (mesh granularity measure)

Simulation parameters in `subduction_forearc.py` are tabulated as follows:

* Mesh dependent parameters:
	* `delta` Dip angle (rad)
	* `z_tau` Shear heating peak temperature depth (km)
	* `z0` Shear heating depth (km)
	* `zh` External heat source depth (km)
	* `Z` Slab depth (km)

* Physical parameters:
	* `rho` Density (kg / m^3)
	* `cp` Specific heat (J / K)
	* `k` Thermal conductivity (W / (m K))
	* `Tm` Temperature at Moho (Centigrade)
	* `T0` Lithosphere age (M yr)
	* `Vn` Slab velocity (cm / yr)
	* `H0` Heat source coefficient (W / (m K))
	* `mu_0` Apparent friction coefficient
	* `Tin_model` Temperature model on the trench-side boundary (`a` for linear; `b` for half-space)
	* `H_model` Radiogenic heat production model (`a` for constant; `b` for exponential decay)
	* `Hsh_model` Shear heating model coefficient. One of:
        * `lineardec`: 1 if `z < z_tau`, linear decrease from 1 to 0 from `z_tau` to `z0`
        * `linearincdec`: linear increase from 0 to 1 from 0 to `z_tau` and linear decrease from 1 to 0 from `z_tau` to `z0`
        * `exp`: linear increase from 0 to 1 from 0 to `z_tau` and exponential decrease `exp(- (z - z_tau) / z_mu )` from `z_tau` to `z0`
        * `mu_only`: shear heating is influenced only by the apparent friction coefficient
    * `mu_model` Apparent friction coefficient model:
        * `linear`: constant value `mu = mu_0`
        * `nonlinear`:  `mu = mu_0` if `T < Tcrit`, else `mu = mu_0 * exp( -(T - Tcrit)/delta_T_mu )`
        
Parameters can be changed from their default values by editing the above two python scripts, or running
with command line arguments, e.g:

```bash
python3 subduction_mesh_generator.py --delta=$(python3 -c "import math; print(math.pi/4.0)")
python3 subduction_forearc.py --delta=$(python3 -c "import math; print(math.pi/4.0)")
```

## Example scenarios ##

The following scenarios are discussed in Supplement 1 of van Keken et al. "Thermal structure of the forearc in 
subduction zones: a comparison of methodologies".

For each of the following scenarios we use the following mesh:

```bash
python3 subduction_mesh_generator.py --delta=$(python3 -c "import math; print(math.pi/4.0)") --z0=80.0 --z_tau=40.0 --Z=100.0
```

###### Case 1
```bash
python3 subduction_forearc.py --delta=$(python3 -c "import math; print(math.pi/4.0)") --mu_0=0.0 --Z=100.0 --H0=0.0 --Tin_model=a --H_model=a
```
###### Case 2
```bash
python3 subduction_forearc.py --delta=$(python3 -c "import math; print(math.pi/4.0)") --mu_0=0.03 --z_tau=40.0 --z0=80.0 --zh=0.0 --Z=100.0 --H0=0.0 --T0=0.0 --Tin_model=a --H_model=a
```
###### Case 3
```bash
python3 subduction_forearc.py --delta=$(python3 -c "import math; print(math.pi/4.0)") --mu_0=0.03 --z_tau=40.0 --z0=80.0 --zh=20.0 --Z=100.0 --H0=1.5e-6 --T0=0.0 --Tin_model=a --H_model=a
```
###### Case 4
```bash
python3 subduction_forearc.py --delta=$(python3 -c "import math; print(math.pi/4.0)") --mu_0=0.03 --z_tau=40.0 --z0=80.0 --zh=20.0 --Z=100.0 --H0=1.5e-6 --T0=10.0 --Tin_model=b --H_model=a
```

###### Reference functionals

Approximate values taken from [*van Keken et al. 2019*](https://doi.org/10.1029/2019GC008334) supporting information Table S1.2

| Case   | T(40km, -40km) |
|--------|----------------|
| 1      | 107.8          |
| 2      | 188.0          |
| 3      | 203.9          |
| 4      | 418.2          |

## Running in parallel ##

The script `subduction_forearc.py` supports parallel computation.
E.g:

```python
mpirun -np 8 python3 subduction_forearc.py
```

## Mesh convergence study ##

Validation of computational results is provided through mesh convergence
studies. To run a mesh convergence study, enable the parameter `conv_study`
and the number of meshes to test convergence `conv_level`. E.g. a convergence
study with 3 levels of refinement (level 0 being the reference mesh), where
each level halves the `lcar` factor:

```bash
python3 subduction_mesh_generator.py --conv_study=True --conv_level=2 --conv_lcar_factor=0.5
python3 subduction_forearc.py --conv_study=True --conv_level=2
```

Example output is of the form

```
hmin: 3.140e+02, Qoi: 1.23724710e+02, DoFs: 80322
hmin: 1.582e+02, Qoi: 1.23713806e+02, DoFs: 319349
hmin: 7.752e+01, Qoi: 1.23711118e+02, DoFs: 1272096
```

where `hmin` is the minimum mesh element diameter, `QoI` is the quantity of
interest (the temperature at (40km, -40km) by default) and `DoFs` is the
number of degrees of freedom in the underlying linear algebra system.


# Docker help #

For general help see the [FEniCS in
Docker](https://fenics.readthedocs.io/projects/containers/en/latest/)
tutorial.


## Sharing files between container and host ##

Share the folder `/home/fenics/shared/` with your current working directory:

```bash
docker run -ti -v $(pwd):/home/fenics/shared -w /home/fenics/shared quay.io/natesime/subduction-zone-forearc-thermal-structure
```

Files created in `/home/fenics/shared/` will be accessible on the current
working directory on your host machine. E.g: after a simulation copy the
temperature field result to the shared directory `sudo cp temperature.*
~/shared`. Then choose your favourite visualisation software on your host
machine, e.g: [ParaView](https://www.paraview.org/).

## Forwarding the container display ##

See the following links for advice on forwarding plots generated within the docker container
to the host machine:

* [Ubuntu](http://wiki.ros.org/docker/Tutorials/GUI)
* [Mac OS X](https://cntnr.io/running-guis-with-docker-on-mac-os-x-a14df6a76efc)


# Contributors #

* Nate J. C. Sime <nsime@carnegiescience.edu>

* Peter E. van Keken

* Ikuko Wada

# Licence #

GNU LGPL, version 3.
