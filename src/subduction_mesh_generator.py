import h5py
import pygmsh
import meshio
import numpy as np
import os, sys

import dolfin as dfn
if dfn.MPI.size(dfn.MPI.comm_world) > 1:
    raise NotImplementedError("Mesh generation not supported in parallel")

# Ouput directory
output_directory = "meshing"

# Parameter set for mesh generation
subd_pset = dfn.Parameters("Subduction_Mesh_Parameterset")
subd_pset.add("delta", np.pi/4.0)  # Dip angle
subd_pset.add("Z", 100.0)          # Slab depth (km)
subd_pset.add("z_tau", 40.0)       # Shear heating peak temperature depth (km)
subd_pset.add("z0", 80.0)          # Shear heating depth (km)
subd_pset.add("lcar", 0.5)         # Characteristic length of mesh

# Generate mesh convergence study meshes
subd_pset.add("conv_study", False)     # Run with mesh convergence study
subd_pset.add("conv_level", 0)         # If running a convergence study, use this many levels
subd_pset.add("conv_lcar_factor", 0.5) # lcar factor between levels
subd_pset.parse(sys.argv)

print("Subduction mesh parameter set:\n" +
      "\n".join(k + (" = %.6e" % subd_pset[k]) for k in subd_pset.keys()))

slab_dip = subd_pset["delta"]         # Slab dip angle
Z = subd_pset["Z"]  # Depth to which the slab will dip on the far right boundary

width = Z / np.tan(slab_dip)  # Width of the geometry

z0 = subd_pset["z0"]    # Depth of shear heating
z_tau = subd_pset["z_tau"]  # Depth of the peak shear heating

# x and y coordinates of shear heating geometry
xsh, ysh = z_tau / np.tan(slab_dip), -z_tau
x0, y0 = z0/np.tan(slab_dip), -z0

# Mesh convergence parameters
convergence_study = subd_pset["conv_study"]
convergence_study_levels = subd_pset["conv_level"]
convergence_study_factor = subd_pset["conv_lcar_factor"]
base_lcar = subd_pset["lcar"]

# Domain labels for above (UPPER) and below (LOWER) the slab
UPPER, LOWER = 1, 2

# Facet labels used in imposition of boundary conditions and shear heating integration terms
BOTTOM, LEFT, RIGHT_BOTTOM, RIGHT_TOP, TOP, SLAB_TOP, SLAB_MID, SLAB_BOT = 3, 4, 5, 6, 7, 8, 9, 10


def generate_subduction_mesh(lcar):
    # Use pygmsh/gmsh to generate the geometry mesh
    geom = pygmsh.built_in.Geometry()

    # Geometry points
    pt_top_left = geom.add_point((0.0, 0.0, 0.0), lcar=lcar)
    pt_top_right = geom.add_point((width, 0.0, 0.0), lcar=lcar)
    pt_slab_depth = geom.add_point((width, -Z, 0.0), lcar=lcar)
    pt_subd_depth = geom.add_point((width, -2 * Z, 0.0), lcar=lcar)
    pt_bottom_left = geom.add_point((0.0, -Z, 0.0), lcar=lcar)

    pt_slab_z0 = geom.add_point((x0, y0, 0.0), lcar=lcar)
    pt_slab_zsh = geom.add_point((xsh, ysh, 0.0), lcar=lcar)

    # Geometry lines
    line_top = geom.add_line(pt_top_left, pt_top_right)
    line_right_top = geom.add_line(pt_top_right, pt_slab_depth)

    line_slab_bot = geom.add_line(pt_slab_depth, pt_slab_z0)
    line_slab_mid = geom.add_line(pt_slab_z0, pt_slab_zsh)
    line_slab_top = geom.add_line(pt_slab_zsh, pt_top_left)

    line_left = geom.add_line(pt_top_left, pt_bottom_left)
    line_bottom = geom.add_line(pt_bottom_left, pt_subd_depth)
    line_right_bottom = geom.add_line(pt_subd_depth, pt_slab_depth)

    # Geometry surfaces
    ll_top = geom.add_line_loop((line_top, line_right_top, line_slab_bot, line_slab_mid, line_slab_top))
    ll_bottom = geom.add_line_loop((line_left, line_bottom, line_right_bottom, line_slab_bot, line_slab_mid, line_slab_top))

    top_surf = geom.add_plane_surface(ll_top)
    bot_surf = geom.add_plane_surface(ll_bottom)

    # Geometry region labels
    geom.add_physical(top_surf, UPPER)
    geom.add_physical(bot_surf, LOWER)

    # Geometry facet labels
    geom.add_physical(line_left, LEFT)
    geom.add_physical(line_bottom, BOTTOM)
    geom.add_physical(line_right_bottom, RIGHT_BOTTOM)
    geom.add_physical(line_right_top, RIGHT_TOP)

    geom.add_physical(line_top, TOP)
    geom.add_physical(line_slab_top, SLAB_TOP)
    geom.add_physical(line_slab_mid, SLAB_MID)
    geom.add_physical(line_slab_bot, SLAB_BOT)

    # Generate mesh
    pygmsh_mesh = pygmsh.generate_mesh(geom, dim=2, prune_z_0=True)
    return pygmsh_mesh


if __name__ == "__main__":
    for level in range((convergence_study_levels+1) if convergence_study else 1):
        print("Generating mesh %d" % level)

        # Generate the subduction mesh using pygmsh/gmsh
        pygmsh_mesh = generate_subduction_mesh(lcar=base_lcar*convergence_study_factor**level)
        points = pygmsh_mesh.points
        cells_triangle = {"triangle": pygmsh_mesh.get_cells_type("triangle")}
        cell_data_triangle = {"gmsh:physical": [pygmsh_mesh.get_cell_data("gmsh:physical", "triangle")]}

        # Extract the facet information (for boundary conditions)
        cells_line = {"line": pygmsh_mesh.get_cells_type("line")}
        cell_data_line = {"gmsh:physical": pygmsh_mesh.get_cell_data("gmsh:physical", "line")}

        # Compute the map of vertex pairs to facet labels (for boundary conditions)
        line_verts_to_id = dict(zip(map(tuple, cells_line["line"]), cell_data_line["gmsh:physical"]))

        # Write the mesh cells and vertices to XDMF file
        mesh_filename = "custom_subduction_mesh_%d.xdmf" % level if convergence_study \
            else "custom_subduction_mesh.xdmf"
        mesh_filename = os.path.join(output_directory, mesh_filename)
        reduced_mesh = meshio.Mesh(pygmsh_mesh.points, cells_triangle, cell_data=cell_data_triangle)
        meshio.write(mesh_filename, reduced_mesh, compression=None)

        # Read in the XDMF file to a dolfin mesh
        mesh = dfn.Mesh()
        dfn.XDMFFile(mesh_filename).read(mesh)

        # Create a facet function to hold facet labels for boundary conditions
        ff = dfn.MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)

        # Populate the facet function with data generated by gmsh
        for f in dfn.facets(mesh):
            vert_idxs = (f.entities(0)[0], f.entities(0)[1])
            if vert_idxs in line_verts_to_id.keys():
                ff[f] = line_verts_to_id[vert_idxs]
            elif tuple(reversed(vert_idxs)) in line_verts_to_id.keys():
                ff[f] = line_verts_to_id[tuple(reversed(vert_idxs))]

        # Output the facet function to XDMF file
        mesh_facet_filename = "custom_subduction_mesh_facets_%d.xdmf" % level if convergence_study \
            else "custom_subduction_mesh_facets.xdmf"
        mesh_facet_filename = os.path.join(output_directory, mesh_facet_filename)
        dfn.XDMFFile(mesh_facet_filename).write(ff)
