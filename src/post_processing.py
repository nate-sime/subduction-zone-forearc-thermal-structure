import numpy as np
import matplotlib.pyplot as plt
import json
import os

# Simple post processing example, collecting all .json results files
# in ./results and outputting slab temperature and surface heat flux

def format_exponent(f, sigfigs=1):
    if f == 0.0:
        return "0"
    return r"%s \times 10^{%d}" % \
           ((("%." + str(sigfigs) + "e") % f).split("e")[0],
            np.floor(np.log10(np.abs(f))))


def plot_slab_temperature(T, z):
    plt.plot(T, z / 1e3)
    plt.xlim((0.0, 700.0))
    plt.ylim((0.0, 80.0))
    plt.xlabel(r"$T \; (\degree\mathrm{C})$")
    plt.ylabel(r"$z \; (\mathrm{km})$")


def plot_surface_flux(q, x):
    plt.plot(x / 1e3, q * 1e3)
    plt.xlabel(r"$x \; (\mathrm{km})$")
    plt.ylabel(r"$q \; (\mathrm{mW} / \mathrm{m}^{2})$")


def plot_slab_Hsh(Hsh, z):
    plt.plot(Hsh, z / 1e3)
    plt.xlim((0.0, 0.1))
    plt.ylim((0.0, 80.0))
    plt.xlabel(r"$H_{sh} \; (\mathrm{W} / {m^3})$")
    plt.ylabel(r"$z \; (\mathrm{km})$")


results_files = list(f for f in os.listdir("results") if f.endswith(".json"))

legends = ["case4", "caseGW"]

for filename in results_files:
    with open(os.path.join("results", filename), "r") as f:
        data = json.load(f)

    plt.figure(1)
    plot_slab_temperature(np.array(data["slab_T"]), np.array(data["slab_z"]))
    # slab_temperature_legends.append(r"$T_0=%s$" % format_exponent(data["T0"]))

    plt.figure(2)
    plot_surface_flux(np.array(data["surface_q"]), np.array(data["surface_x"]))
    # surface_flux_legends.append(r"$T_0=%s$" % format_exponent(data["T0"]))

    plt.figure(3)
    plot_slab_Hsh(np.array(data["slab_Hsh"]), np.array(data["slab_z"]))
    # surface_flux_legends.append(r"$T_0=%s$" % format_exponent(data["T0"]))

plt.figure(1)
plt.legend(legends)
plt.title("slab temperature")

plt.figure(2)
plt.legend(legends)
plt.title("surface heat flux")

plt.figure(3)
plt.legend(legends)
plt.title("slab heating")

plt.show()