import sys, os

from dolfin import *
import ufl
import numpy as np

# Interior facet integrals require ghosts to know about shared facets between
# processes
parameters["ghost_mode"] = "shared_facet"
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
parameters["std_out_all_processes"] = False
parameters["allow_extrapolation"] = True

# Define a Nonlinear problem to assemble the residual and Jacobian
class Problem(NonlinearProblem):
    def __init__(self, a, L, bcs):
        self.a = a
        self.L = L
        self.bcs = bcs
        NonlinearProblem.__init__(self)

    def F(self, b, x):
        assemble(self.L, tensor=b)
        for bc in self.bcs:
            bc.apply(b, x)

    def J(self, A, x):
        assemble(self.a, tensor=A)
        for bc in self.bcs:
            bc.apply(A)

# Use a customised Newton solver
class CustomSolver(NewtonSolver):
    def __init__(self):
        NewtonSolver.__init__(self, mesh.mpi_comm(),
                              PETScKrylovSolver(), PETScFactory.instance())

    def solver_setup(self, A, P, problem, iteration):
        self.linear_solver().set_operator(A)

        PETScOptions.set("ksp_type", "preonly")
        PETScOptions.set("pc_type", "lu")
        PETScOptions.set("pc_factor_mat_solver_type", "mumps")

        self.linear_solver().set_from_options()

    # def update_solution(self, x, dx, relaxation_parameter, nonlinear_problem, iteration):
    #     tau = 1.0
    #     theta = min(sqrt(2.0*tau/norm(dx, norm_type="l2", mesh=mesh)), 1.0)
    #     info("Newton damping parameter: %.3e" % theta)
    #     x.axpy(-theta, dx)

continuation_parameters = []

class ContinuationParameter:

    def __init__(self, vals):
        vals = vals.strip().replace("[", "").replace("]", "")
        self.vals = tuple(map(float, vals.split(",")))
        self.constant = Constant(self.vals[0])
        continuation_parameters.append(self)


# Mark the subdomains of the mesh above and below the slab, along with
# the boundary markers for boundary conditions
UPPER, LOWER = 1, 2
BOTTOM, LEFT, RIGHT_BOTTOM, RIGHT_TOP, TOP, SLAB_TOP, SLAB_MID, SLAB_BOT = 3, 4, 5, 6, 7, 8, 9, 10

# Geometry dependent parameters
subd_pset = Parameters("Subduction_Parameterset")

# Default values
# Mesh dependent parameters
subd_pset.add("delta", np.pi/4.0)  # Dip angle
subd_pset.add("z_tau", 40.0)       # Peak shear heating depth (km)
subd_pset.add("z0", 80.0)          # Shear heating depth (km)
subd_pset.add("zh", 20.0)          # External heat source depth (km)
subd_pset.add("Z", 100.0)          # Slab depth (km)
subd_pset.add("conv_study", False) # Run with mesh convergence study
subd_pset.add("conv_level", 2)     # If running a convergence study, use this many levels

# Model parameters
subd_pset.add("Tin_model", "a", {"a", "b"}) # Temperature model on the trench-side boundary (`a` for linear; `b` for half-space)
subd_pset.add("H_model", "a", {"a", "b"})   # Radiogenic heat production model (`a` for constant; `b` for exponential decay)

# Shear heating model coefficient which consists of:
# * lineardec: 1 if z < z_tau, linear decrease from 1 to 0 from z_tau to z0
# * linearincdec: linear increase from 0 to 1 from 0 to z_tau and linear decrease from 1 to 0 from z_tau to z0
# * exp: linear increase from 0 to 1 from 0 to z_tau and exponential decrease exp(- (z - z_tau) / z_mu ) from z_tau to z0
# * mu_only: shear heating is influenced only by the apparent friction coefficient
# * strainrate: shear heating is influenced by the strainrate model
subd_pset.add("Hsh_model", "lineardec", {"lineardec", "linearincdec", "exp", "mu_only", "strainrate"})

# Apparent friction coefficient model:
# * linear: constant value mu = mu_0
# * nonlinear:  mu = mu_0 if T < Tcrit, else mu = mu_0 * exp( -(T - Tcrit)/delta_T_mu )
# * strainrate:  strainrate model TODO: document this
subd_pset.add("mu_model", "linear", {"linear", "nonlinear", "strainrate"})

# Physical quantities
subd_pset.add("rho", 3300.0)       # Density (kg / m^3)
subd_pset.add("cp", 939.39)        # Specific heat J / (kg K)
subd_pset.add("k", 3.1)            # Thermal conductivity
subd_pset.add("Tm", 1350.0)        # Temperature at Moho (Centigrade!)
subd_pset.add("T0", 50.0)          # Lithosphere age (M yr)
subd_pset.add("V", 5.0)            # Slab velocity (cm / yr)
subd_pset.add("H0", 1.5e-6)        # W / (m K)
subd_pset.add("mu_0", "0.0")       # Apparent friction coefficient
subd_pset.add("delta_z_mu", 10e3)  # Coefficient of shear heating exponential decay
subd_pset.add("Tcrit",  600.0)
subd_pset.add("delta_T_mu", 60.0)

subd_pset.add("AA", 2e-4)           #
subd_pset.add("nn", 1.9)            # Strainrate exponent
subd_pset.add("E", 141e3)           # Activation energy (J/mol)
subd_pset.add("w", 500.0)           # Channel width (m)

# XDMF output data (pointdata or fefunction)
subd_pset.add("output_format", "xdmf", {"xdmf", "vtu"})
subd_pset.add("xdmf_output", "pointdata", {"pointdata", "fefunction"})
subd_pset.add("output_dir", "results")

# Parse any user parameters and report
subd_pset.parse(sys.argv)
def formatter(k):
    v = subd_pset[k]
    return k + " = " + (v if isinstance(v, str) else ("%.6e" % v))
info("Subduction parameter set:\n" + "\n".join(map(formatter, subd_pset.keys())))

# Get parameters for simulation
delta = subd_pset["delta"]

# Mesh dependent parameters which are converted from km -> m
z_tau = Constant(1e3*subd_pset["z_tau"])
z0 = Constant(1e3*subd_pset["z0"])
zh = Constant(1e3*subd_pset["zh"])
Z = Constant(1e3*subd_pset["Z"])

convergence_study = subd_pset["conv_study"]
convergence_study_levels = subd_pset["conv_level"]
rho = Constant(subd_pset["rho"])
cp = Constant(subd_pset["cp"])
Tm = Constant(subd_pset["Tm"])
k = Constant(subd_pset["k"])
T0 = subd_pset["T0"]
Vn = subd_pset["V"]
H0 = Constant(subd_pset["H0"])
mu_0 = ContinuationParameter(subd_pset["mu_0"]).constant
delta_z_mu = Constant(subd_pset["delta_z_mu"])
Tcrit = Constant(subd_pset["Tcrit"])
delta_T_mu = Constant(subd_pset["delta_T_mu"])

AA = Constant(subd_pset["AA"])
nn = Constant(subd_pset["nn"])
E = Constant(subd_pset["E"])
w = Constant(subd_pset["w"])

T_in_model = subd_pset["Tin_model"]
H_model = subd_pset["H_model"]
Hsh_model = subd_pset["Hsh_model"]
mu_model = subd_pset["mu_model"]

output_format = subd_pset["output_format"]
xdmf_output = subd_pset["xdmf_output"]

# Physical constants
R = Constant(8.315)


def solve_forearc_problem(mesh, domain_labels, facet_labels):
    # Spatial coordinate and depth, z
    x = SpatialCoordinate(mesh)
    z = -x[1]

    # Heat equation function space and variables.
    S = FunctionSpace(mesh, "CG", 1)
    T, s = Function(S), TestFunction(S)
    T.rename("T", "T")

    g = 9.80665  # Gravitational acceleration
    kappa = k / (rho * cp)  # Thermal diffusivity m^2/s

    # Convert to seconds
    A = Constant(T0 * 1e6 * 365.25 * 24.0 * 3600.0)  # Lithosphere age (s)
    V = Constant(Vn * 0.01 / (365.25 * 24.0 * 3600.0))  # Velocity (m/s)

    # Radiogenic heating term
    Hcc_code = "H0*exp(-x[1]/zh)" if H_model == "b" else "(-x[1]) < zh ? H0 : 0.0"
    Hcc = Expression(Hcc_code, degree=4, H0=H0, zh=zh)

    # Prescribed velocity field
    u = Expression(("V*cos(delta)", "-V*sin(delta)"), delta=delta, degree=0, V=V)

    # The strongly enforced Dirichlet boundary conditions
    Tin_code = "Tm*erf(-x[1]/(2.0*sqrt(kappa*A)))" if T_in_model == "b" else "-x[1]*Tm/Z"
    Tin = Expression(Tin_code, Tm=Tm, kappa=float(kappa), A=A, Z=Z, degree=4)
    T.interpolate(Tin)

    # Measure the number of unknowns in the linear system
    info("Number of DoFs: %d" % S.dim())

    bcs = [DirichletBC(S, Tin, facet_labels, LEFT),
           DirichletBC(S, Constant(0.0), facet_labels, TOP),
           DirichletBC(S, Tm, facet_labels, BOTTOM)]

    # Integration measures used in the weak formulation
    ds = Measure("ds", subdomain_data=facet_labels)
    dS = Measure("dS", subdomain_data=facet_labels)
    dx = Measure("dx", subdomain_data=domain_labels)

    n = FacetNormal(mesh)

    # Shear heating functions
    linear_increase = Expression("x[1]/(-z_tau)", degree=1, z_tau=z_tau)
    linear_decrease = Expression("(1.0 - (x[1] - (-z_tau))/((-z0) - (-z_tau)))", degree=1, z_tau=z_tau, z0=z0)
    exponential_decrease = Expression("exp(-(-x[1] - z_tau) / delta_z_mu)", degree=3, z_tau=z_tau, delta_z_mu=delta_z_mu)

    if mu_model == "linear":
        mu = mu_0
    elif mu_model == "nonlinear":
        mu = conditional(ufl.lt(T("+"), Tcrit), mu_0, mu_0*exp(-(T("+") - Tcrit)/delta_T_mu))
    elif mu_model == "strainrate":
        P = rho * g * z
        edot = V/w
        edotn = edot**(1.0/nn)
        An = AA**(-1.0/nn)
        tauF = mu_0 * P
        tauV = ufl.Min(Constant(1e20), Constant(1e6)*An*exp(E / (nn * R * (T("+") + 273.15))) * edotn)
        tau = tauV * ufl.tanh(tauF / tauV)
    else:
        raise NotImplementedError("mu model not implemented")

    if Hsh_model == "lineardec":
        Hsh_upper = mu * rho * g * z * V
        Hsh_lower = mu * rho * g * V * z_tau * linear_decrease
    elif Hsh_model == "linearincdec":
        Hsh_upper = mu * rho * g * z * V * linear_increase
        Hsh_lower = mu * rho * g * V * z_tau * linear_decrease
    elif Hsh_model == "exp":
        Hsh_upper = mu * rho * g * z * V * linear_increase
        Hsh_lower = mu * rho * g * V * z_tau * exponential_decrease
    elif Hsh_model == "mu_only":
        Hsh = mu * rho * g * z * V
    elif Hsh_model == "strainrate":
        Hsh = tau * V
    else:
        raise NotImplementedError("Shear heating model not implemented")

    # Semilinear residual formulation
    F = -rho * cp * T * dot(u, grad(s)) * dx(LOWER) \
        + rho * cp * T * dot(u, n) * s * ds(RIGHT_BOTTOM) \
        + dot(k*grad(T), grad(s)) * dx - Hcc * s * dx(UPPER)

    if mu_model == "linear":
        F -= Hsh_upper * s("+") * dS(SLAB_TOP) \
             + Hsh_lower * s("+") * dS(SLAB_MID)
        Hsh = ufl.conditional(z < z_tau, Hsh_upper, Hsh_lower)
    elif mu_model == "nonlinear" or mu_model == "strainrate":
        F -= Hsh * s("+") * dS((SLAB_TOP, SLAB_MID, SLAB_BOT))
    else:
        raise NotImplementedError("mu model not implemented")

    # Symbolic Gateaux derivative of the semilinear residual
    J = derivative(F, T)

    # Construct the problem (matrix) assembler and solver
    problem = Problem(J, F, bcs)
    solver = CustomSolver()

    count = 0
    for param in continuation_parameters:
        for val in param.vals:
            param.constant.assign(val)
            solver.solve(problem, T.vector())
            output_functions(T, count, slab_functions={"slab_Hsh": Hsh})
            count += 1

    return T


def output_functions(T, count, slab_functions={}):
    mesh = T.function_space().mesh()

    # Write the temperature field to file
    if output_format == "vtu":
        File(os.path.join(subd_pset["output_dir"], "temperature%d.pvd" % count)) << T
    elif output_format == "xdmf":
        with XDMFFile(os.path.join(subd_pset["output_dir"], "temperature%d.xdmf" % count)) as f:
            if xdmf_output == "pointdata":
                f.write(T)
            elif xdmf_output == "fefunction":
                f.write_checkpoint(T, "T")

    # Measure the temperature along the slab
    y_vals = np.linspace(1e-3, 100.0e3, 100)
    points = np.array([[y/np.tan(delta), -y] for y in y_vals])
    T_vals = evaluate_function(points, T).flatten()

    slab_data = {}
    for name, slab_func_ufl in slab_functions.items():
        func = project(slab_func_ufl, T.function_space())
        slab_data[name] = evaluate_function(points, func).flatten().tolist()

    # Measure the heat flux at the surface boundary
    T_flux = project(-k*T.dx(1), T.function_space())
    x_vals = np.linspace(0.0, 100.0e3, 100)
    points = np.array([[x, 0.0] for x in x_vals])
    T_flux_vals = evaluate_function(points, T_flux).flatten()

    # Measure the temperature at point (40km, -40km)
    measured_T = evaluate_function(np.array([[40e3, -40e3]]), T)

    # Output the quantities of interest
    if MPI.rank(mesh.mpi_comm()) == 0:
        import matplotlib.pyplot as plt
        import json

        print("T(40km, -40km) = %f" % measured_T[0])

        # Output to json format
        results = {
            "slab_z": y_vals.tolist(),
            "slab_T": T_vals.tolist(),
            "surface_x": x_vals.tolist(),
            "surface_q": T_flux_vals.tolist(),
            "measured_T": measured_T.tolist(),
        }
        results.update(slab_data)
        results.update(dict(map(lambda k: (k, subd_pset[k]), subd_pset.keys())))

        output_filename = os.path.join(subd_pset["output_dir"], "metadata%d.json" % count)
        with open(output_filename, "w") as f:
            json.dump(results, f, indent=4, separators=(",", ": "))

        # Output to csv format
        slab_temp_data = np.vstack((y_vals, T_vals)).T
        np.savetxt(os.path.join(subd_pset["output_dir"], "slab_temperature_data%d.csv" % count),
                   slab_temp_data,
                   delimiter=",", header="z, T_slab(z)")

        surface_flux_data = np.vstack((x_vals, T_flux_vals)).T
        np.savetxt(os.path.join(subd_pset["output_dir"], "surface_flux_data%d.csv" % count),
                   surface_flux_data,
                   delimiter=",", header="x, Q_s(x)")


# Compute quantities of interest (qoi). E.g. measure the temperature at position x0 = (50km, -50km).
# We need to find on which process x0 resides before computing the interpolant of the FE function.
def evaluate_function(x, u):
    comm = mesh.mpi_comm()
    if comm.size == 1:
        return np.fromiter(map(u, x), dtype=np.double)

    u_shape = u.value_shape()[0] if len(u.value_shape()) > 0 else 1
    local_measured = np.zeros((x.shape[0], u_shape))
    for j in range(x.shape[0]):
        x0 = Point(*x[j])
        u_cell, distance = mesh.bounding_box_tree().compute_closest_entity(x0)
        local_measured[j, :] = u(x0) if distance < DOLFIN_EPS else None

    gathered_u_vals = comm.gather(local_measured)

    u_vals = np.zeros((x.shape[0], 1))
    if comm.rank == 0:
        gathered_u_vals = np.hstack(gathered_u_vals)
        for j in range(gathered_u_vals.shape[0]):
            u_vals[j] = np.fromiter((y for y in gathered_u_vals[j] if not np.isnan(y)), dtype=np.double)

    if len(u_vals) == 1:
        u_vals = u_vals[0]
    return u_vals


if convergence_study:
    # Mesh convergence measures
    values = []
    dofs = []
    hsize = []

    for lvl in range(convergence_study_levels + 1):
        info("Convergence study level %d" % lvl)

        # Read the mesh and convert to SI units
        mesh = Mesh()
        XDMFFile("meshing/custom_subduction_mesh_%d.xdmf" % lvl).read(mesh)
        domains = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)
        XDMFFile("meshing/custom_subduction_mesh_%d.xdmf" % lvl).read(domains)
        ff = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
        XDMFFile("meshing/custom_subduction_mesh_facets_%d.xdmf" % lvl).read(ff)
        mesh.coordinates()[:] *= 1e3  # km -> m

        T = solve_forearc_problem(mesh, domains, ff)

        # Measure the temperature at point (40km, -40km)
        measured_T = evaluate_function(np.array([[40e3, -40e3]]), T)

        dofs.append(T.function_space().dim())
        values.append(measured_T)
        hsize.append(T.function_space().mesh().hmin())

    if MPI.rank(mesh.mpi_comm()) == 0:
        import matplotlib.pyplot as plt

        print("\n".join("hmin: %.3e, QoI: %.8e, DoFs: %d" % (hmin, qoi, dof) for (hmin, qoi, dof) in zip(hsize, values, dofs)))

        plt.figure(1)
        info("%s\n%s" % (str(dofs), str(values)))
        plt.semilogx(dofs, values)
        plt.xlabel(r"DoFs")
        plt.ylabel(r"QoI")

        plt.figure(2)
        plt.plot(hsize, values)
        plt.xlabel(r"$h_\mathrm{min}$")
        plt.ylabel(r"QoI")

        plt.show()

else:
    # Read the mesh and convert to SI units
    mesh = Mesh()
    XDMFFile("meshing/custom_subduction_mesh.xdmf").read(mesh)
    domains = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)
    XDMFFile("meshing/custom_subduction_mesh.xdmf").read(domains)
    ff = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
    XDMFFile("meshing/custom_subduction_mesh_facets.xdmf").read(ff)
    mesh.coordinates()[:] *= 1e3  # km -> m

    T = solve_forearc_problem(mesh, domains, ff)
        #
        # # Create plots
        # plt.figure(1)
        # plt.plot(x_vals/1e3, T_flux_vals*1e3)
        # plt.xlabel("x (km)")
        # plt.ylabel("q (m W / m^2)")
        # plt.grid()
        #
        # plt.figure(2)
        # plt.plot(T_vals, y_vals/1e3)
        # plt.xlim((0.0, 600.0))
        # plt.ylim((0.0, 100.0))
        # plt.xlabel("T (C)")
        # plt.ylabel("z (km)")
        # plt.grid()
        #
        # plt.show()
